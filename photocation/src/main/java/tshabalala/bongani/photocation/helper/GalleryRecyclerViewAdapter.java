package tshabalala.bongani.photocation.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.photocation.FullScreenActivity;
import tshabalala.bongani.photocation.ImageViewerActivity;
import tshabalala.bongani.photocation.R;

/**
 * Created by Bongani on 2018/05/28.
 */

public class GalleryRecyclerViewAdapter extends RecyclerView.Adapter<GalleryRecyclerViewHolder>
{

    private Context context;


    private List<String> mLikelyPlaceNames;
    private GeoDataClient mGeoDataClient;
    private int currentPhotoIndex = 0;
    private List<File> photosDataList;
    Bitmap photoBitmap;
    String address = "";

    public GalleryRecyclerViewAdapter(Context context, List<String> mLikelyPlaceNames) {
        this.context = context;
        this.mLikelyPlaceNames = mLikelyPlaceNames;

    }

    @Override
    public GalleryRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_card_list, null);
        GalleryRecyclerViewHolder rcv = new GalleryRecyclerViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final GalleryRecyclerViewHolder holder,final int position) {

       mGeoDataClient = Places.getGeoDataClient(context, null);

        final String placeName = mLikelyPlaceNames.get(position);

        holder.txtName.setText(placeName);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File dir = new File(Environment.getExternalStorageDirectory().getPath()+"/photocation/"+placeName);
                boolean found = (dir.mkdirs() || dir.isDirectory());
                photosDataList = new ArrayList<>();
                if(found) {
                    File[] files = dir.listFiles();
                    for (File f : files) {
                        photosDataList.add(f);
                    }
                }
                Intent intent = new Intent(context, ImageViewerActivity.class);
                intent.putExtra("photos", (Serializable) photosDataList);
                intent.putExtra("path", dir.getPath());
                intent.putExtra("placeName", placeName);
                context.startActivity(intent);


            }
        });




    }

    @Override
    public int getItemCount() {
        return mLikelyPlaceNames.size();
    }

}
