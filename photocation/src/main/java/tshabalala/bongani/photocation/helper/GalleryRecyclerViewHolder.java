package tshabalala.bongani.photocation.helper;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tshabalala.bongani.photocation.R;

/**
 * Created by Bongani on 2018/05/28.
 */

public class GalleryRecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView txtName;
    public TextView txtAddress;
    public ImageView imageView;
    public CardView cardView;
    public LinearLayout mLinearLayout;
    public FrameLayout frameLayout;

    public GalleryRecyclerViewHolder(View itemView) {
        super(itemView);
        txtName = (TextView)itemView.findViewById(R.id.name);
        txtAddress = (TextView)itemView.findViewById(R.id.address);
        imageView = (ImageView)itemView.findViewById(R.id.imageOverlay);
        cardView = (CardView) itemView.findViewById(R.id.card_view);
        mLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll);

        frameLayout = (FrameLayout)itemView.findViewById(R.id.overlay);

    }
}
