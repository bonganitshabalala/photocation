package tshabalala.bongani.photocation.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.photocation.FullScreenActivity;
import tshabalala.bongani.photocation.MainActivity;
import tshabalala.bongani.photocation.R;

/**
 * Created by Bongani on 2018/05/25.
 */

public class PhotoRecyclerViewAdapter extends RecyclerView.Adapter<PhotoRecyclerViewHolder>{

    private Context context;

    private List<String> mLikelyPlaceIds;
    private List<String> mLikelyPlaceNames;
    private List<String> mLikelyPlaceAddress;
    private List<String> mLikelyPlaceAttributes;
    private GeoDataClient mGeoDataClient;
    private int currentPhotoIndex = 0;
    private List<PlacePhotoMetadata> photosDataList;
    Bitmap photoBitmap;
    String address = "";

    public PhotoRecyclerViewAdapter(Context context, List<String> mLikelyPlaceIds, List<String> mLikelyPlaceNames, List<String> mLikelyPlaceAddress, List<String> mLikelyPlaceAttributes) {
        this.context = context;
        this.mLikelyPlaceIds = mLikelyPlaceIds;
        this.mLikelyPlaceNames = mLikelyPlaceNames;
        this.mLikelyPlaceAddress = mLikelyPlaceAddress;
        this.mLikelyPlaceAttributes = mLikelyPlaceAttributes;
    }

    @Override
    public PhotoRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_card_list, null);
        PhotoRecyclerViewHolder rcv = new PhotoRecyclerViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final PhotoRecyclerViewHolder holder,final int position) {

        Log.d("TAG IN Recycler", "mLikelyPlaceIds " + mLikelyPlaceIds.size() + " mLikelyPlaceNames " + mLikelyPlaceNames.size()+ " mLikelyPlaceAddress " + mLikelyPlaceAddress.size()+ " mLikelyPlaceAttrs " + mLikelyPlaceAttributes.size());
        mGeoDataClient = Places.getGeoDataClient(context, null);

        final String placeId = mLikelyPlaceIds.get(position);
        final String placeName = mLikelyPlaceNames.get(position);
        final String addres = mLikelyPlaceAddress.get(position);
        String attrs = mLikelyPlaceAttributes.get(position);
       // getPhotoMetadata(placeId);
        Log.d("addres","Address "+addres);

        holder.txtName.setText(placeName);
        holder.txtAddress.setText(addres);
        final Task<PlacePhotoMetadataResponse> photoResponse =
                mGeoDataClient.getPlacePhotos(placeId);

        photoResponse.addOnCompleteListener
                (new OnCompleteListener<PlacePhotoMetadataResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlacePhotoMetadataResponse> task) {
                        currentPhotoIndex = 0;
                        photosDataList = new ArrayList<>();
                        PlacePhotoMetadataResponse photos = task.getResult();
                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();

                        for(PlacePhotoMetadata photoMetadata : photoMetadataBuffer){
                            photosDataList.add(photoMetadataBuffer.get(0).freeze());

                        }

                        if(photosDataList.size() > 0) {
                            Task<PlacePhotoResponse> photoResponse = mGeoDataClient.getPhoto(photosDataList.get(currentPhotoIndex));
                            photoResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoResponse>() {
                                @Override
                                public void onComplete(@NonNull Task<PlacePhotoResponse> task) {
                                    PlacePhotoResponse photo = task.getResult();
                                    photoBitmap = photo.getBitmap();
                                    holder.imageView.invalidate();
                                    holder.imageView.setImageBitmap(photoBitmap);

                                    saveImages(photoBitmap,addres);
                                }
                            });
                        }

                        photoMetadataBuffer.release();
                    }
                });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, FullScreenActivity.class);
                intent.putExtra("placeId",placeId);
                intent.putExtra("placeName",placeName);
                intent.putExtra("placeAddress",addres);
                context.startActivity(intent);


            }
        });




    }

    public interface OnItemClickListener{
        void onItemClick();
    }

    @Override
    public int getItemCount() {
        return mLikelyPlaceNames.size();
    }

    private void getPhotoMetadata(String placeId) {

        final Task<PlacePhotoMetadataResponse> photoResponse =
                mGeoDataClient.getPlacePhotos(placeId);

        photoResponse.addOnCompleteListener
                (new OnCompleteListener<PlacePhotoMetadataResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlacePhotoMetadataResponse> task) {
                        currentPhotoIndex = 0;
                        photosDataList = new ArrayList<>();
                        PlacePhotoMetadataResponse photos = task.getResult();
                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();

                        for(PlacePhotoMetadata photoMetadata : photoMetadataBuffer){
                            photosDataList.add(photoMetadataBuffer.get(0).freeze());
                        }

                        photoMetadataBuffer.release();

                        displayPhoto();
                    }
                });
    }

    private void displayPhoto(){

        if(photosDataList.isEmpty() || currentPhotoIndex > photosDataList.size() - 1){
            return;
        }
        getPhoto(photosDataList.get(currentPhotoIndex));

    }

    private void getPhoto(PlacePhotoMetadata photoMetadata){
        Task<PlacePhotoResponse> photoResponse = mGeoDataClient.getPhoto(photoMetadata);
        photoResponse.addOnCompleteListener(new OnCompleteListener<PlacePhotoResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlacePhotoResponse> task) {
                PlacePhotoResponse photo = task.getResult();
                photoBitmap = photo.getBitmap();


            }
        });
    }

    public void saveImages(Bitmap bitmap, String address)
    {
        String [] arrayAddress = address.split(",");
        String locationCity = arrayAddress[0];
        String locationName = arrayAddress[2];
        File dir = new File(Environment.getExternalStorageDirectory().getPath()+"/photocation/"+locationCity);
        boolean found = (dir.mkdirs() || dir.isDirectory());
        if(found) {
            File mypath = new File(dir,locationName+".png");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


}
