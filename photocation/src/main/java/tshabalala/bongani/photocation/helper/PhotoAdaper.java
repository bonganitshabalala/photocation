package tshabalala.bongani.photocation.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tshabalala.bongani.photocation.R;

/**
 * Created by Bongani on 2018/05/28.
 */

public class PhotoAdaper extends ArrayAdapter<File> {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;
    private List<File> fileList;
    private String path;


    private static class ViewHolder {
        private TextView mSideBarText = null;
        private TextView mSideBarText1 = null;
        private ImageView mImageView = null;

    }

    public PhotoAdaper(Activity context, int textViewResourceId, List<File> fileList,String path){
        super(context, textViewResourceId, fileList);
        this.mContext = context;
        this.fileList = fileList;
        this.path = path;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final File file = fileList.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.photo_adapter, parent, false);

            vh.mSideBarText = (TextView) contentView.findViewById(R.id.sideBarText);
            vh.mSideBarText1 = (TextView) contentView.findViewById(R.id.sideBarText1);
            vh.mImageView = (ImageView) contentView.findViewById(R.id.imageView);

            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM HH:mm");
        String date = dateFormat.format(file.lastModified());
        vh.mSideBarText.setText(file.getName());
        vh.mSideBarText1.setText(date);
        try {

            File f = new File(path, file.getName());
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            vh.mImageView.setImageBitmap(b);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }



        return (contentView);
    }


}
