package tshabalala.bongani.photocation.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import tshabalala.bongani.photocation.R;
import tshabalala.bongani.photocation.helper.GridSpacingItemDecoration;
import tshabalala.bongani.photocation.helper.PhotoRecyclerViewAdapter;

/**
 * Created by Bongani on 2018/05/28.
 */

public class PhotoDialogFragment extends DialogFragment {

    private RecyclerView recyclerView;
    private static List<String> mLikelyPlaceIds;
    private static List<String> mLikelyPlaceNames;
    private static List<String> mLikelyPlaceAddress;
    private static List<String> mLikelyPlaceAttributes;
    private PhotoRecyclerViewAdapter photoRecyclerViewAdapter;
    private GridLayoutManager gridLayoutManager;
    private String path = "photocation";
    File dir;

    public static PhotoDialogFragment newInstance(List<String> strings, List<String> strings1,List<String> strings2, List<String> strings3) {
        Bundle bundle = new Bundle();
        mLikelyPlaceIds = strings;
        mLikelyPlaceNames = strings1;
        mLikelyPlaceAddress = strings2;
        mLikelyPlaceAttributes = strings3;
        PhotoDialogFragment sampleFragment = new PhotoDialogFragment();
        sampleFragment.setArguments(bundle);
        return sampleFragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_photo, null);

            Toolbar toolbar = (Toolbar) dialogView.findViewById(R.id.toolbar);
            toolbar.setTitle("Current location places");
            recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
            gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            recyclerView.setLayoutManager(gridLayoutManager);

            int spanCount = 3; // 3 columns
            int spacing = 50; // 50px
            boolean includeEdge = true;
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
            final Bundle args = getArguments();
            Log.d("TAG", "mLikelyPlaceIds " + mLikelyPlaceIds.size() + " mLikelyPlaceNames " + mLikelyPlaceNames.size()+ " mLikelyPlaceAddress " + mLikelyPlaceAddress.size());
            photoRecyclerViewAdapter = new PhotoRecyclerViewAdapter(getActivity(),mLikelyPlaceIds,mLikelyPlaceNames,mLikelyPlaceAddress,mLikelyPlaceAttributes);
            recyclerView.setAdapter(photoRecyclerViewAdapter);
            photoRecyclerViewAdapter.notifyDataSetChanged();

            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity(), R.style.MyWhiteAlertDialogStyle)
                    .setView(dialogView)
                    .setPositiveButton("Cancel", null)
                    .create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button b = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            dialog.dismiss();
                        }
                    });
                }
            });
            return alertDialog;
        } else {
            return super.onCreateDialog(savedInstanceState);
        }
    }







}
