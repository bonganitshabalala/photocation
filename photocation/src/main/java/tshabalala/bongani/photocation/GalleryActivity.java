package tshabalala.bongani.photocation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.photocation.helper.BottomNavigationViewHelper;
import tshabalala.bongani.photocation.helper.GalleryRecyclerViewAdapter;
import tshabalala.bongani.photocation.helper.GridSpacingItemDecoration;

/**
 * Created by Bongani on 2018/05/25.
 */

public class GalleryActivity extends AppCompatActivity {

    private final static String TAG = GalleryActivity.class.getName();
    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private String path = "photocation";
    List<String> locationCityPlaceName = new ArrayList<>();
    File dir;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_gallery);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Gallery");
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        int spanCount = 3; // 3 columns
        int spacing = 50; // 50px
        boolean includeEdge = true;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        dir = new File(Environment.getExternalStorageDirectory().getPath()+"/"+path);
        boolean found = (dir.mkdirs() || dir.isDirectory());
        if(found) {
            File [] files = dir.listFiles();
            for(File f : files)
            {
                locationCityPlaceName.add(f.getName());
            }

        }

        GalleryRecyclerViewAdapter galleryRecyclerViewAdapter = new GalleryRecyclerViewAdapter(this,locationCityPlaceName);
        recyclerView.setAdapter(galleryRecyclerViewAdapter);
        galleryRecyclerViewAdapter.notifyDataSetChanged();


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.ic_location:

                        Intent intent = new Intent(GalleryActivity.this,MainActivity.class);
                        startActivity(intent);

                        break;

                    case R.id.ic_gallery:

                        break;

                }

                return false;
            }
        });
    }

    private void loadImageFromStorage(String path)
    {

        String locationCity;
        String locationPlace;


    }
}
