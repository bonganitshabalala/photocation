package tshabalala.bongani.photocation;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.List;

import tshabalala.bongani.photocation.helper.PhotoAdaper;

/**
 * Created by Bongani on 2018/05/28.
 */

public class ImageViewerActivity extends AppCompatActivity {

    private List<File> photosLocationNames;
    private ListView listView;
    File dir;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.photo_list);
        String placeName = getIntent().getStringExtra("placeName");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(placeName + " - Images");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ImageViewerActivity.this,GalleryActivity.class));

            }
        });
        StrictMode.VmPolicy.Builder policy = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(policy.build());
        listView = (ListView) findViewById(R.id.listView);
        photosLocationNames = (List<File>) getIntent().getSerializableExtra("photos");
        final String path = getIntent().getStringExtra("path");


        final PhotoAdaper adapter = new PhotoAdaper(ImageViewerActivity.this,
                        android.R.layout.simple_list_item_1,
                         photosLocationNames,path);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                File file = photosLocationNames.get(i);
                Intent intent = new Intent(ImageViewerActivity.this,ImageActivity.class);
                intent.putExtra("file",file);
                intent.putExtra("path",path);
                startActivity(intent);
            }
        });

    }
}
